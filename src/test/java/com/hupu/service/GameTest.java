package com.hupu.service;

import com.hupu.service.Impl.GameServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GameTest {
    @Autowired
    private GameServiceImpl gameService;
    
    @Test
    public void testGame1() {
        System.out.println("=====Test Bean Create=====");
        System.out.println(gameService);
    }
    
    @Test
    public void testGame2() {
        System.out.println("=====Test Query By Page=====");
        gameService.queryAllByLimit(10, 20).forEach(System.out::println);
    }
}

package com.hupu.service;

import com.alibaba.fastjson.JSON;
import com.hupu.service.Impl.TeamServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TeamTest {
    @Autowired
    private TeamServiceImpl teamService;
    
    @Test
    public void testGet() {
        teamService.queryAllByLimit(0, 100).stream().map(JSON::toJSONString).forEach(System.out::println);
    }
    
    
}

package com.hupu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.hupu.dao")
public class HupuApplication {

    public static void main(String[] args) {
        SpringApplication.run(HupuApplication.class, args);
    }

}

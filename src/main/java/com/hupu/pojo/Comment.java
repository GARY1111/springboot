package com.hupu.pojo;

public class Comment {
    
    
    private Long commentId;
    
    private Integer userId;
    
    private Integer gameId;
    
    private String commentInfo;
    
    private String commentTime;
    
    public Comment() {
    }
    
    public Comment(Long commentId, Integer userId, Integer gameId, String commentInfo, String commentTime) {
        this.commentId = commentId;
        this.userId = userId;
        this.gameId = gameId;
        this.commentInfo = commentInfo;
        this.commentTime = commentTime;
    }
    
    public Long getCommentId() {
        return commentId;
    }
    
    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }
    
    public Integer getUserId() {
        return userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    
    public Integer getGameId() {
        return gameId;
    }
    
    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }
    
    public String getCommentInfo() {
        return commentInfo;
    }
    
    public void setCommentInfo(String commentInfo) {
        this.commentInfo = commentInfo;
    }
    
    public String getCommentTime() {
        return commentTime;
    }
    
    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }
    
}
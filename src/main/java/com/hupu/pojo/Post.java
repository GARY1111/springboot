package com.hupu.pojo;

import java.io.Serializable;

public class Post implements Serializable {
    
    
    private Integer postId;
    
    private String postTitle;
    
    private String postContent;
    
    private String postTime;
    
    private Integer postCommentnum;
    //发表该消息的用户id 
    private Integer adminId;
    
    
    public Integer getPostId() {
        return postId;
    }
    
    public void setPostId(Integer postId) {
        this.postId = postId;
    }
    
    public String getPostTitle() {
        return postTitle;
    }
    
    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }
    
    public String getPostContent() {
        return postContent;
    }
    
    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }
    
    public String getPostTime() {
        return postTime;
    }
    
    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }
    
    public Integer getPostCommentnum() {
        return postCommentnum;
    }
    
    public void setPostCommentnum(Integer postCommentnum) {
        this.postCommentnum = postCommentnum;
    }
    
    public Integer getAdminId() {
        return adminId;
    }
    
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }
    
}